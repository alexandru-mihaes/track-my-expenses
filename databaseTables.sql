-- Database name :TME

CREATE TABLE users (
  id       int          not null AUTO_INCREMENT PRIMARY KEY,
  username varchar(200) not null,
  password varchar(200) not null,
  email    varchar(200) not null
);

CREATE TABLE `Categories` (
  `id`   int         NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `Groups` (
  `id`       int         NOT NULL AUTO_INCREMENT,
  `name`     varchar(30) NOT NULL,
  `owner_id` int         NOT NULL,
  PRIMARY KEY (
    `id`
  ),
  FOREIGN KEY (`owner_id`) REFERENCES users (`id`)
);

CREATE TABLE `Transactions` (
  `id`          int          NOT NULL AUTO_INCREMENT,
  `name`        varchar(100) NOT NULL,
  `amount`      int          NOT NULL,
  `type`        varchar(3)   NOT NULL,
  `message`     text         NOT NULL,
  `date`        date         NOT NULL,
  `user_id`     int          NOT NULL,
  `category_id` int          NOT NULL,
  `group_id`    int,
  PRIMARY KEY (
    `id`
  ),
  FOREIGN KEY (`user_id`) REFERENCES users (id),
  FOREIGN KEY (`category_id`) REFERENCES categories (id),
  FOREIGN KEY (`group_id`) REFERENCES groups (id)
);

CREATE TABLE `Members` (
  `id`       int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `user_id`  int NOT NULL,
  `group_id` int NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users (id),
  FOREIGN KEY (group_id) REFERENCES groups (id)
);

CREATE TABLE `Notifications` (
  `id`       int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `user_id`  int NOT NULL,
  `group_id` int NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users (id),
  FOREIGN KEY (group_id) REFERENCES groups (id)
);

/* Default categories */
INSERT INTO categories (name) VALUES ('food');
INSERT INTO categories (name) VALUES ('entertainment');
INSERT INTO categories (name) VALUES ('salary');
INSERT INTO categories (name) VALUES ('bills');
INSERT INTO categories (name) VALUES ('others');