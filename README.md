![Logo](assets/images/logo.png)

![Preview](assets/images/preview.png)

## How to host

1. You have to create a MySQL database named `tme`.

2. The tables that you have to create are in `databaseTables.sql` file.