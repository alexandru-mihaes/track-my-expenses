<?php
class Group {
    public $id = null;
    public $name;
    public $owner;

    public function __construct($id, $name, $owner) {
      $this->id = $id;
      $this->name = $name;
      $this->owner = $owner;
    }
}