<?php

class Date {
    public $date;
    public $day;
    public $month;
    public $monthVal;
    public $year;
    public $dayWord;
    public $total;

    public function equals($other) {
        return ($this->date == $other->date);
    }
}