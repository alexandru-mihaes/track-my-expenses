<?php

class Notification {
    public $id = null;
    public $group;
    public $receiver;

    public function __construct($id, $group, $receiver) {
        $this->id = $id;
        $this->group = $group;
        $this->receiver = $receiver;
    }
}