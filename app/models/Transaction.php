<?php

class Transaction {
	public $id = null;
	public $title;
	public $message;
	public $amount;
	public $type;

	public $user;
	public $group;
	public $category;

	public $date;
}