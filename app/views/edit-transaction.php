<?php
$html = file_get_contents("pages/edit-transaction.html", FILE_USE_INCLUDE_PATH);

$footer = ComponentFactory::getFooter();
$html = str_replace("{{footer}}", $footer, $html);

$groups = ComponentFactory::getOptions($data[0],"G");
$html = str_replace("{{groups-options}}", $groups, $html);

$categories = ComponentFactory::getOptions($data[1],"C");
$html = str_replace("{{categories-options}}", $categories, $html);

$transaction = TransactionsService::getByID($_GET["id"]);
$html = str_replace("{{title}}", $transaction->title, $html);
$html = str_replace("{{message}}", $transaction->message, $html);
$html = str_replace("{{amount}}", $transaction->amount, $html);
$html = str_replace("{{id}}", $_GET["id"], $html);

$date = $transaction->date->year . '-' . $transaction->date->monthVal . '-' . $transaction->date->day;
$html = str_replace("{{date}}", $date, $html);

$dom = EditTransaction::setDefaultDropdows($html,$transaction);
print $dom->saveXML();