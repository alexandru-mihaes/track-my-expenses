<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <meta name="description"
          content="Track my expenses helps you to manage your expenses. It's that very simple, get it today!">
    <meta name="keywords" content="expense manager, track expenses, financial assistant">
    <title>Forgot your password? - Track My Expenses</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="../public/css/main.css">
    <link rel="stylesheet" type="text/css" href="../public/css/authentification/sign-in.css">
    <link rel="stylesheet" type="text/css" href="../public/css/authentification/forgot-pass.css">
</head>

<body>

<div id="auth-form-wrapper" class="centerElementsWithin">
    <div class="container">
        <div id="auth-form" class="centerElementsWithin">
            <div id="auth-width">
                <div id="logo-wrapper" class="centerElementsWithin">
                    <a id="logoRef" href="home">
                        <img id="logo" src="../public/images/logo.png" alt="logo">
                    </a>
                </div>
                <p id="auth-title">Track My Expenses</p>

                <p id="password-message"><i>Forgot your password?</i></p>

               <form action="forgotpass" method="POST">
                    <input class="field" type="email" name="email" placeholder="Email" required>
                    <button class="submit-button-wrapper" type="submit" name="submit">
                        <p>Email me</p>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>

<footer>
    <div class="container">
        <div id="footer-bar">
            <div id="footer-bar-text">
                Made with
                <svg class="heart" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12">
                    <path d="M9 2C7.312 2 6 3.5 6 3.5S4.687 2 3 2C1.312 2 0 3.312 0 5c0 3.188 3.75 3.012 6 6 2.25-2.988 6-2.812 6-6 0-1.688-1.313-3-3-3z"></path>
                </svg>
                in Iași
            </div>
            <div class="footer-bar-text">
                &copy; 2018 Track My Expenses
            </div>
        </div>
    </div>
</footer>

</body>

</html>