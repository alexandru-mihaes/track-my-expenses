<?php

class ComponentFactory
{
    public static function getTransactionData($transactions) 
    {
        $html = self::getOverview($transactions) . self::getTransactionCards($transactions);
        return $html;
    }
    public static function getTransactionCards($transactions)
    {
        if (isset($transactions)) {
            $html = "";

            $date = $transactions[0]->date;
            unset($transactionsDay);
            $transactionsDay[] = $transactions[0];
            unset($transactions[0]);

            foreach ($transactions as $transaction) {
                if ($date->date == $transaction->date->date) {
                    $transactionsDay[] = $transaction;
                } else {
                    if (count($transactionsDay) > 0) {
                        $html = $html . ComponentFactory::getDayCard($date, $transactionsDay);
                    }
                    $date = $transaction->date;
                    unset($transactionsDay);
                    $transactionsDay[] = $transaction;
                }
            }
            if (count($transactionsDay) > 0) {
                $html = $html . ComponentFactory::getDayCard($date, $transactionsDay);
            }
            return $html;
        } else {
            return file_get_contents(__DIR__ . '/components/no-transactions.html');
        }
    }

    public static function getDayCard($date, $transactions)
    {
        $card = file_get_contents(__DIR__ . '/components/transaction.html');

        $card = str_replace("{{day}}", $date->day, $card);
        $card = str_replace("{{month}}", $date->month, $card);
        $card = str_replace("{{year}}", $date->year, $card);
        $card = str_replace("{{day-word}}", $date->dayWord, $card);

        $items = "";
        $total = 0;

        foreach ($transactions as $transaction) {
            $item = file_get_contents(__DIR__ . '/components/transaction-item.html');
            $item = str_replace("{{transaction-id}}", (string)$transaction->id, $item);
            $item = str_replace("{{name}}", $transaction->title, $item);
            $item = str_replace("{{username}}", $transaction->user, $item);
            $item = str_replace("{{value}}", $transaction->amount, $item);
            $item = str_replace("{{message}}", $transaction->message, $item);
            $item = str_replace("{{type}}", $transaction->type, $item);

            $items = $items . $item;
            if ($transaction->type == "out") {
                $total -= $transaction->amount;
            } else if ($transaction->type == "in") {
                $total += $transaction->amount;
            }
        }

        if ($total < 0) {
            $card = str_replace("{{value}}", -$total, $card);
            $card = str_replace("{{value-class}}", "out", $card);
        } else {
            $card = str_replace("{{value}}", $total, $card);
            $card = str_replace("{{value-class}}", "in", $card);
        }
        $card = str_replace("{{items}}", $items, $card);

        return $card;
    }

    public static function getHeader()
    {
        $authentificated = Session::isAuthentificated();
        if ($authentificated) {
            $html = file_get_contents(__DIR__ . '/components/header-in.html');

            if (isset(Session::getUser()->username)) {
                $username = Session::getUser()->username;
            } else {
                $username = "Account";
            }
            $html = str_replace("{{username}}", $username, $html);
        } else {
            $html = file_get_contents(__DIR__ . '/components/header-out.html');
        }

        return $html;
    }

    public static function getFooter()
    {
        $html = file_get_contents(__DIR__ . '/components/footer.html');

        return $html;
    }

    public static function getOptions($options, $idPrefix)
    {
        $html = "";
        if(isset($options)){
            foreach ($options as $option) {
                $html = $html . '<option id="' . $idPrefix . $option->id . '" value="' . $option->id . '">' . ucfirst($option->name) . "</option>";
            }
            return $html;
        }
        return null;
    }

    public static function getOverview($transactions)
    {
        $html = file_get_contents(__DIR__ . '/components/overview.html');

        $inflow = 0;
        $outflow = 0;
        if(isset($transactions)) {
            foreach($transactions as $transaction) {
                if($transaction->type == 'in') {
                    $inflow += $transaction->amount;
                } else if($transaction->type == 'out') {
                    $outflow += $transaction->amount;
                }
            }
        }


        $sum = abs($inflow - $outflow);
        if ($inflow < $outflow) {
            $type = "out";
        } else {
            $type = "in";
        }

        $html = str_replace("{{inflow}}", $inflow, $html);
        $html = str_replace("{{outflow}}", $outflow, $html);
        $html = str_replace("{{sumVal}}", $sum, $html);
        $html = str_replace("{{sumType}}", $type, $html);

        return $html;
    }

    public static function getGroups()
    {
        $html = "";

        $groups = GroupsService::getList(Session::getUser());

        foreach ($groups as $group) {
            $line = file_get_contents(__DIR__ . '/components/group-radio.html');
            $groupName = isset($_GET['group']) ? mysqli_real_escape_string(Database::getConnection(), $_GET['group']) : 'Personal';
            
            if (strtolower($group->name) == strtolower($groupName)) {
                $line = str_replace("{{checked}}", "checked", $line);
            } else {
                $line = str_replace("{{checked}}", "", $line);
            }
            $line = str_replace("{{name}}", $group->name, $line);
            $line = str_replace("{{id}}", $group->id, $line);

            $html = $html . $line;
        }

        return $html;
    }

    public static function getCategories() {
        $html = '<div class="radio"><input id="all" type="radio" value="all" name="category" {{checked}}><label for="all" class="radio-label">all</label></div>';

        $categories = CategoriesService::getListByUser(Session::getUser()->id, true);
        $categoryName = isset($_GET['category']) ? mysqli_real_escape_string(Database::getConnection(), $_GET['category']) : 'all';
        if ('all' == strtolower($categoryName)) {
            $html = str_replace("{{checked}}", "checked", $html);
        } else {
            $html = str_replace("{{checked}}", "", $html);
        }

        foreach($categories as $category) {
            $line = file_get_contents(__DIR__ . '/components/category-radio.html');
        
            if (strtolower($category->name) == strtolower($categoryName)) {
                $line = str_replace("{{checked}}", "checked", $line);
            } else {
                $line = str_replace("{{checked}}", "", $line);
            }
            $line = str_replace("{{name}}", $category->name, $line);

            $html = $html . $line;
        }

        return $html;
    }

    public static function getGroupsRecords() {
        $html = "";

        $groups = GroupsService::getList(Session::getUser());

        foreach ($groups as $group) {
            if('personal' != strtolower($group->name)){
                $line = file_get_contents(__DIR__ . '/components/user-settings/add-record.html');
                
                $line = str_replace("{{function}}", "deletegroup", $line);
                $line = str_replace("{{id}}", $group->id, $line);
                $line = str_replace("{{value}}", ucfirst($group->name), $line);

                $html = $html . $line;
            }
        }

        return $html;
    }

    public static function getCategoriesRecords() {
        $html = "";

        $categories = CategoriesService::getListByUser(Session::getUser()->id, false);

        foreach($categories as $category) {
            $line = file_get_contents(__DIR__ . '/components/user-settings/add-record.html');

            $line = str_replace("{{function}}", "deletecategory", $line);
            $line = str_replace("{{id}}", $category->id, $line);
            $line = str_replace("{{value}}", $category->name, $line);

            $html = $html . $line;
        }

        return $html;
    }

    public static function getNotifications() {
        $html = "";

        $notifications = NotificationsService::getListByUser(Session::getUser()->id);

        foreach($notifications as $notification) {
            $line = file_get_contents(__DIR__ . '/components/user-settings/notification-row.html');

            $line = str_replace("{{group}}", $notification->group->name, $line);

            $html = $html . $line;
        }

        return $html;
    }

    public static function getDropdownGroups() {
        $html = "";

        $groups = GroupsService::getListOwned(Session::getUser());

        $html = ComponentFactory::getOptions($groups, "G");
        return $html;
    }
}
