<?php
$html = file_get_contents("pages/add-transaction.html", FILE_USE_INCLUDE_PATH);

$footer = ComponentFactory::getFooter();
$html = str_replace("{{footer}}", $footer, $html);

$groups = ComponentFactory::getOptions($data[0],"G");
$html = str_replace("{{groups-options}}", $groups, $html);

$categories = ComponentFactory::getOptions($data[1],"C");
$html = str_replace("{{categories-options}}", $categories, $html);

echo $html;