<?php
    $html = file_get_contents("pages/wallet.html", FILE_USE_INCLUDE_PATH);

    $componentFactoryPath = dirname(__FILE__) . '\ComponentFactory.php';

    
    $header = ComponentFactory::getHeader();
    $html = str_replace("{{header}}", $header, $html);

    $groups = ComponentFactory::getGroups();
    $html = str_replace("{{groups}}", $groups, $html);

    $categories = ComponentFactory::getCategories();
    $html = str_replace("{{categories}}", $categories, $html);

    $footer = ComponentFactory::getFooter();
    $html = str_replace("{{footer}}", $footer, $html);

    echo $html;