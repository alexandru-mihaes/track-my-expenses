<?php
    $html = file_get_contents("pages/user-settings.html", FILE_USE_INCLUDE_PATH);

    $componentFactoryPath = dirname(__FILE__) . '\ComponentFactory.php';


    $header = ComponentFactory::getHeader();
    $html = str_replace("{{header}}", $header, $html);

    $html = str_replace("{{user}}", Session::getUser()->username, $html);

    $addNotifications = ComponentFactory::getNotifications();
    $html = str_replace("{{notification-rows}}", $addNotifications, $html);

    $addRecordsGroups = ComponentFactory::getGroupsRecords();
    $html = str_replace("{{add-records-groups}}", $addRecordsGroups, $html);

    $addRecordsCategories = ComponentFactory::getCategoriesRecords();
    $html = str_replace("{{add-records-categories}}", $addRecordsCategories, $html);

    $addDropdownGroups = ComponentFactory::getDropdownGroups($data[0],"G");
    $html = str_replace("{{dropdown-owned-groups}}", $addDropdownGroups, $html);

    $footer = ComponentFactory::getFooter();
    $html = str_replace("{{footer}}", $footer, $html);

    echo $html;