<?php
class MembersService {
    public static function getGroups($user) {
        $conn = Database::getConnection();

        $sql = $conn->prepare("SELECT M.id as mid, user_id, group_id, G.id as gid, name, owner_id
                FROM Members as M JOIN Groups as G ON group_id = G.id
                WHERE user_id = ?");
        $sql->bind_param("i",$user->id);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        if($result == null)
            echo "result is null";

        while($row = mysqli_fetch_assoc($result)) {
            $group = new Group($row['gid'],$row['name'],$row['owner_id']);
            $list[] = $group;
        }

        return isset($list) ? $list : null;
    }

    public static function join($user,$group) {
        $conn = Database::getConnection();

        $sql = $conn->prepare("INSERT INTO Members (id,user_id,group_id)
                VALUES(null,?,?)");
        $sql->bind_param("ii",$user->id,$group->id);
        $sql->execute();
        $sql->close();
    }

    public static function remove($user,$group,$owning) {
        $conn = Database::getConnection();
        $trasactionQuery = array();

        if($owning==true){
            $sql = $conn->prepare("DELETE FROM Members WHERE group_id = ?");
            $sql->bind_param("i",$group->id);
            $groupID=$group->id;
        }
        else {
            $sql = $conn->prepare("DELETE FROM Members WHERE user_id = ? AND group_id = ?");
            $sql->bind_param("ii",$user->id,$group->id);
            $groupID=$group->id;
            $userID=$user->id;
        }
        TransactionsService::deleteByGroup($groupID,$userID);
        $sql->execute();
        $sql->close();
    }
}