<?php

class NotificationsService {
    public static function getListByUser($userID) {
        $notifications = array();

        $conn = Database::getConnection();

        $sql = $conn->prepare("SELECT n.id id, user_id, g.id gid ,g.name name, g.owner_id owner FROM notifications n JOIN groups g ON n.group_id = g.id WHERE user_id = ?");
        $sql->bind_param("i",$userID);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        while($row = mysqli_fetch_assoc($result)) {
            $notifications[] = new Notification($row['id'], new Group($row['gid'], $row['name'], $row['owner']), $row['user_id']);
        }

        return $notifications;
    }

    public static function add($notification) {
        $user = Session::getUser();

        $conn = Database::getConnection();
        $sql = $conn->prepare("INSERT INTO Notifications (id, user_id, group_id) VALUES (null, ?, ?)");
        $sql->bind_param("si",$notification->receiver->id,$notification->group->id);
        $sql->execute();
        $sql->close();
    }

    public static function delete($notification) {
        $conn = Database::getConnection();

        $sql = $conn->prepare("DELETE FROM Notifications WHERE id=?");
        $sql->bind_param("i",$notification->id);
        $sql->execute();
        $sql->close();
    }

    public static function getByReceiverAndGroup($receiver,$group) {
        $conn = Database::getConnection();

        $sql = $conn->prepare("SELECT n.id nid, user_id, group_id,
                                      g.id gid, name, owner_id
                               FROM Notifications n 
                               JOIN Groups g ON n.group_id = g.id
                               WHERE n.user_id=? AND g.name=?");
        $sql->bind_param("is",$receiver->id,$group->name);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        if($row = mysqli_fetch_assoc($result)) {
            $owner = new User($row['owner_id'], null, null);
            $group = new Group($row['gid'], $row['name'], $owner);
            $notification = new Notification($row['nid'], $group, $receiver);
        }
        else {
            $notification = null;
        }

        return $notification;
    }

    public static function exists($notification) {
        $conn = Database::getConnection();

        $sql = $conn->prepare("SELECT * FROM notifications WHERE user_id=? AND group_id=?");
        $sql->bind_param("si",$notification->receiver->id,$notification->group->id);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        if(mysqli_num_rows($result) > 0){
            return true;
        }
        else {
            return false;
        }
    }
}