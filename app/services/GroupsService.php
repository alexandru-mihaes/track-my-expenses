<?php
class GroupsService {
    public static function getByName($user,$name) {
        $conn = Database::getConnection();

        $owner_id = $user->id;
        $sql = $conn->prepare("SELECT g.id gid,name,owner_id FROM Groups g LEFT JOIN Members m ON g.id=m.group_id 
        WHERE (user_id = ? OR owner_id=?) AND name=?");
        $sql->bind_param("iis",$owner_id,$owner_id, $name);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        if($row = mysqli_fetch_assoc($result)) {
            return new Group($row['gid'],$row['name'],$row['owner_id']);
        } else {
            return null;
        }
    }
    public static function getList($user) {
        $conn = Database::getConnection();

        $owner_id = $user->id;
        $sql = $conn->prepare("SELECT Groups.id id, name, owner_id FROM Groups JOIN Members ON group_id = Groups.id WHERE user_id = ?");
        $sql->bind_param("i",$owner_id);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        while($row = mysqli_fetch_assoc($result)) {
            $groups[] = new Group($row['id'],$row['name'],$row['owner_id']);
        }

        return $groups;
    }

    public static function getListOwned($user) {
        $conn = Database::getConnection();

        $owner_id = $user->id;
        $sql = $conn->prepare("SELECT * FROM Groups WHERE owner_id = ? AND LOWER(name) <> 'personal'");
        $sql->bind_param("i",$owner_id);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        while($row = mysqli_fetch_assoc($result)) {
            $groups[] = new Group($row['id'],$row['name'],$row['owner_id']);
        }

        return isset($groups) ? $groups : null;
    }
    
    public static function add($group) {
        $user = $group->owner;

        $conn = Database::getConnection();
        $sql = $conn->prepare("INSERT INTO Groups (id, name, owner_id) 
                VALUES(null, ?, ?)");
        $sql->bind_param("si",$group->name, $user->id);
        $sql->execute();
        $sql->close();

        $group = self::getByName($user,$group->name);
        MembersService::join($user,$group);
    }

    public static function delete($group,$owning) {
        $conn = Database::getConnection();
        MembersService::remove(Session::getUser(), $group, $owning);
        $sql = $conn->prepare("DELETE FROM Groups WHERE id=?");
        $sql->bind_param("i",$group->id);
        $sql->execute();
        $sql->close();
    }

    public static function getByID($groupID) {
        $conn = Database::getConnection();

        $sql = $conn->prepare("SELECT * FROM Groups WHERE id=?");
        $sql->bind_param("i",$groupID);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        if($row = mysqli_fetch_assoc($result)) {
            return new Group($row['id'],$row['name'],$row['owner_id']);
        } else {
            return null;
        }
    }
}