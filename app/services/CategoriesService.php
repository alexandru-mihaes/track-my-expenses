<?php
class CategoriesService {
    public static function getByName($name) {
        $conn = Database::getConnection();
        $name = mysqli_real_escape_string($conn,$name);

        $sql = $conn->prepare("SELECT * FROM Categories WHERE name=?");
        $sql->bind_param("s",$name);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        if($row = mysqli_fetch_assoc($result)) {
            return new Category($row['id'],$row['name']);
        } else {
            return null;
        }
    }

    public static function getList() {
        $conn = Database::getConnection();

        $sql = $conn->prepare("SELECT * FROM Categories");
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        while($row = mysqli_fetch_assoc($result)) {
            $categories[] = new Category($row['id'],$row['name']);
        }

        return $categories;
    }

    public static function getListByUser($userID, $default) {
        $categories = array();

        $conn = Database::getConnection();

        if($default == true) {
            $sql = $conn->prepare("SELECT * FROM categories WHERE user_id=? or user_id is null");
        }
        else {
            $sql = $conn->prepare("SELECT * FROM categories WHERE user_id=?");
        }
        $sql->bind_param("s",$userID);

        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        while($row = mysqli_fetch_assoc($result)) {
            $categories[] = new Category($row['id'],$row['name']);
        }

        return $categories;
    }

    public static function add($category) {
        $user = Session::getUser();

        $conn = Database::getConnection();
        $sql = $conn->prepare("INSERT INTO Categories (id, name, user_id) 
                VALUES(null, ?, ?)");
        $sql->bind_param("si",$category->name,$user->id);
        $sql->execute();
        $sql->close();
    }

    public static function delete($category) {
        $user = Session::getUser();

        TransactionsService::deleteByCategory($category->id);
        $conn = Database::getConnection();
        $sql = $conn->prepare("DELETE FROM Categories WHERE id=?");
        $sql->bind_param("i",$category->id);
        $sql->execute();
        $sql->close();
    }
}

