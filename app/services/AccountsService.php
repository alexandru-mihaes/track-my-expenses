<?php
class AccountsService {
    public static function getUser($identifier,$password) {
        $conn = Database::getConnection();

        $identifier = mysqli_real_escape_string($conn, $identifier);
        $password = mysqli_real_escape_string($conn, $password);

        $sql = $conn->prepare("SELECT id, username, password, email FROM users WHERE (email=? OR username=?)");
        $sql->bind_param("ss",$identifier,$identifier);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        $row = mysqli_fetch_assoc($result);

        if (mysqli_num_rows($result) > 0 && password_verify($password, $row["password"])) {
            $user = new User($row['id'],$row['username'],$row['email']);
            return $user;
        } else {
            return null;
        }
    }

    public static function getUserByID($userID) {
        $conn = Database::getConnection();

        $sql = $conn->prepare("SELECT id, username, email FROM users WHERE id=?");
        $sql->bind_param("i",$userID);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        $row = mysqli_fetch_assoc($result);

        if (mysqli_num_rows($result) > 0) {
            $user = new User($row['id'],$row['username'],$row['email']);
            return $user;
        } else {
            return null;
        }
    }

    public static function exists($username) {
        $conn = Database::getConnection();

        $username = mysqli_real_escape_string($conn, $username);

        $sql = $conn->prepare("SELECT id, username, email FROM users WHERE username=?");
        $sql->bind_param("s",$username);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        $row = mysqli_fetch_assoc($result);

        if (mysqli_num_rows($result) > 0) {
            $user = new User($row['id'],$row['username'],$row['email']);
            return $user;
        } else {
            return false;
        }
    }

    public static function add($username,$password,$email) {
        $conn = Database::getConnection();

        $email = mysqli_real_escape_string($conn, $email);
        $username = mysqli_real_escape_string($conn, $username);
        $password = mysqli_real_escape_string($conn, $password);

        $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

        $sql = $conn->prepare("INSERT INTO users (email,username,password) VALUES (?, ?, ?)");
        $sql->bind_param("sss", $email, $username, $hashedPassword);
        
        $sql->execute();
        $sql->close();
    }

   public static function isUserAvailable($username) {
        $conn = Database::getConnection();

        $username = mysqli_real_escape_string($conn, $username);
        $sql = $conn->prepare("SELECT * FROM Users WHERE username=?");
        $sql->bind_param("s",$username);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        if(mysqli_num_rows($result) > 0) {
            return false;
        } else {
            return true;
        }
    }
    public static function isEmailAvailable($email) {
        $conn = Database::getConnection();

        $email = mysqli_real_escape_string($conn, $email);
        $sql = $conn->prepare("SELECT * FROM Users WHERE email=?");
        $sql->bind_param("s",$email);
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        if(mysqli_num_rows($result) > 0) {
            return false;
        } else {
            return true;
        }
    }
}

