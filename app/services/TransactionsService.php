<?php
class TransactionsService {

    public static function getByFilters($user,$group,$category) {
        $conn = Database::getConnection();

        if(!isset($group->name)) {

        }

        if(strtolower($group->name) == 'personal'){
            $sql = $conn->prepare("SELECT *, DATE_FORMAT(date, '%W') as dayword,
                            DATE_FORMAT(date, '%d') as day,
                            DATE_FORMAT(date, '%M') as month,
                            DATE_FORMAT(date, '%Y') as year
                    FROM Transactions WHERE user_id=? ORDER BY date DESC");
            $sql->bind_param("s",$user->id);
        }
        else {
            $sql = $conn->prepare("SELECT *, DATE_FORMAT(date, '%W') as dayword,
                            DATE_FORMAT(date, '%d') as day,
                            DATE_FORMAT(date, '%M') as month,
                            DATE_FORMAT(date, '%Y') as year
                    FROM Transactions ORDER BY date DESC");
        }
        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        while($row = mysqli_fetch_assoc($result)) {
            $user = AccountsService::getUserByID($row["user_id"]);

            $transaction = new Transaction();
            $transaction->id = $row["id"];
            $transaction->title = $row["name"] ;
	        $transaction->amount = $row["amount"];
	        $transaction->message = $row["message"];
            $transaction->user = $user->username;
            $transaction->type = $row["type"];

            $date = new Date();
            $date->date = $row["date"];
            $date->dayWord = $row["dayword"];
            $date->day = $row["day"];
            $date->month = $row["month"];
            $date->year = $row["year"];
            $transaction->date = $date;

            $accepted = true;
            if(isset($group->id) && $row["group_id"] != $group->id) {
                $accepted = false;
            }
            if(isset($category->id) && $row["category_id"] != $category->id) {
                $accepted = false;
            }
                
            if($accepted) {
                $list[] = $transaction;
            }
        }
        return isset($list) ? $list : null;
    }

    public static function add($transaction) {
        $conn = Database::getConnection();
        $sql = $conn->prepare("INSERT INTO Transactions (id, name, message, amount, type, user_id, group_id, category_id, date)
                VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?)");
        $sql->bind_param("ssisiiis",$transaction->title,$transaction->message,$transaction->amount,$transaction->type,
                        $transaction->user,$transaction->group,$transaction->category,$transaction->date);
        $sql->execute();
        $sql->close();
    }

    public static function edit($transaction) {
        $conn = Database::getConnection();

        $sql = $conn->prepare("UPDATE Transactions SET name=?,message=?, amount=?, type=?, user_id=?, 
                group_id=?, category_id=?, date=? WHERE id=?");
        $sql->bind_param("ssisiiisi",$transaction->title,$transaction->message,$transaction->amount,$transaction->type,
        $transaction->user,$transaction->group,$transaction->category,$transaction->date,$transaction->id);
        $sql->execute();
        $sql->close();
    }

    public static function remove($id) {
        $conn = Database::getConnection();

        $sql = $conn->prepare("DELETE FROM Transactions WHERE id=?");
        $sql->bind_param("i",$id);

        $sql->execute();
        $sql->close();
    }

    public static function getByID($id) {
        $conn = Database::getConnection();
        $transaction = new Transaction();

        $sql = $conn->prepare("SELECT *, DATE_FORMAT(date, '%m') as monthVal,
                          DATE_FORMAT(date, '%W') as dayword,
                          DATE_FORMAT(date, '%d') as day, 
                          DATE_FORMAT(date, '%M') as month, 
                          DATE_FORMAT(date, '%Y') as year
                FROM transactions WHERE id=?");
        $sql->bind_param("i",$id);

        $sql->execute();
        $result = $sql->get_result();
        $sql->close();

        $row = mysqli_fetch_assoc($result);

        if (mysqli_num_rows($result) > 0) {
            $transaction = new Transaction();
            $transaction->id = $row["id"];
            $transaction->title = $row["name"];
	        $transaction->amount = $row["amount"];
	        $transaction->message = $row["message"];
            $transaction->user = $row["user_id"];
            $transaction->type = $row["type"];
            $transaction->group = $row["group_id"];
            $transaction->category = $row["category_id"];

            $date = new Date();
            $date->date = $row["date"];
            $date->dayWord = $row["dayword"];
            $date->day = $row["day"];
            $date->month = $row["month"];
            $date->monthVal = $row["monthVal"];
            $date->year = $row["year"];
            $transaction->date = $date;
        }

        return $transaction;
    }

    public static function deleteByGroup($groupID,$userID) {
        $conn = Database::getConnection();
        if(isset($userID)) {
            $sql = $conn->prepare("DELETE FROM Transactions WHERE group_id=? AND user_id=?");
            $sql->bind_param("ii",$groupID,$userID);
        }
        else {
            $sql = $conn->prepare("DELETE FROM Transactions WHERE group_id=?");
            $sql->bind_param("i",$groupID);
        }
    
        $sql->execute();
        $sql->close();

    }

    public static function deleteByCategory($categoryID) {
        $conn = Database::getConnection();

        $sql = $conn->prepare("DELETE FROM Transactions WHERE category_id=?");
        $sql->bind_param("i",$categoryID);
        $sql->execute();
        $sql->close();
    }
}