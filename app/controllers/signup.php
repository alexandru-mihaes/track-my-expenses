<?php

class SignUp extends Controller
{
    public function index()
    {
        $this->view('sign-up');
    }

    public function register()
    {
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];

        $userAvailable = AccountsService::isUserAvailable($username);
        $emailAvailable = AccountsService::isEmailAvailable($email);

        if (!$userAvailable) {
            header("Location: signup?signup=usernametaken");
            exit();
        } else if (!$emailAvailable) {
            header("Location: signup?signup=emailtaken");
            exit();
        } else {
            AccountsService::add($username,$password,$email);
            $user = AccountsService::getUser($username,$password);
            GroupsService::add(new Group(null,'Personal',$user));

            header("Location: home?signup=succes");
            exit();
        }
    }
}