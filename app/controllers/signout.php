<?php
class Signout extends Controller {
    public function index($name = ''){
        Session::signout();
        header("Location: home");
        exit();
    }
}