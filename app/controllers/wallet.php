<?php

class Wallet extends Controller {
    public function index() {
        $authentificated = Session::isAuthentificated();
        
        if ($authentificated) {
            $this->view('wallet');
        } else {
            $this->view('no-login');
        }
    }
}