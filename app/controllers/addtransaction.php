<?php

class AddTransaction extends Controller {
    public function index(){
        $groups = GroupsService::getList(Session::getUser());
        $categories = CategoriesService::getList();
        $this->view('add-transaction',[$groups,$categories]);
    }

    public static function submit() {
        $transaction = new Transaction();
        $conn = Database::getConnection();

        $transaction->title = mysqli_real_escape_string($conn,$_POST["title"]);
        $transaction->message = mysqli_real_escape_string($conn,$_POST["message"]);
        $transaction->amount = mysqli_real_escape_string($conn,$_POST["amount"]);
        $transaction->type = mysqli_real_escape_string($conn,$_POST["type"]);

        $transaction->user = Session::getUser()->id;
        $transaction->group = mysqli_real_escape_string($conn,$_POST["group"]);
        $transaction->category = mysqli_real_escape_string($conn,$_POST["category"]);

        $transaction->date = mysqli_real_escape_string($conn,$_POST["date"]);

        TransactionsService::add($transaction);
        header("Location: wallet");
    }
}