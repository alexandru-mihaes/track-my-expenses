<?php

class SignIn extends Controller
{
    public function index()
    {
        $this->view('sign-in');
    }

    public function signin()
    {
        if (isset($_POST['submit'])) {
            $user = AccountsService::getUser($_POST['identifier'],$_POST['password']);

            if(isset($user)) {
                Session::signin($user);
                header("Location: wallet");
                exit();
            } else {
                header("Location: signin?credentials=incorrect");
                exit();
            }
        }
    }

}
