<?php

class UserSettings extends Controller
{
    public function index()
    {
        $groups = GroupsService::getList(Session::getUser());
        $this->view('user-settings', [$groups]);
    }

    public static function addGroup()
    {
        if (isset($_POST['submit'])) {
            $group = new Group(null, $_POST["group"], Session::getUser());
            if (!self::checkGroupExist($group->name)) {
                GroupsService::add($group);
            }
        }
        header("Location: usersettings");
    }

    public static function deleteGroup()
    {
        if (isset($_POST['submit'])) {
            $group = GroupsService::getByID($_GET["id"]);
            if ($group->owner == Session::getUser()->id) {
                $owning = true;
            } else {
                $owning = false;
            }
            GroupsService::delete($group, $owning);
        }
        header("Location: usersettings");
    }

    public static function addCategory()
    {
        if (isset($_POST['submit'])) {
            $category = new Category(null, $_POST["category"]);
            $category->user_id = Session::getUser()->id;
            if (!self::checkCategoryExists($category->name)) {
                CategoriesService::add($category);
            }
        }
        header("Location: usersettings");
    }

    public static function deleteCategory()
    {
        if (isset($_POST['submit'])) {
            $category = new Category($_GET["id"], "");
            $category->user_id = Session::getUser()->id;
            CategoriesService::delete($category);
        }
        header("Location: usersettings");
    }

    private static function checkGroupExist($name)
    {
        $groups = GroupsService::getList(Session::getUser());

        foreach ($groups as $group) {
            if (strtolower($group->name) == strtolower($name)) {
                return true;
            }
        }
        return false;
    }

    private static function checkCategoryExists($name)
    {
        $categories = CategoriesService::getListByUser(Session::getUser()->id, true);

        foreach ($categories as $category) {
            if (strtolower($category->name) == strtolower($name)) {
                return true;
            }
        }
        return false;
    }

    public function answerNotification()
    {
        if (isset($_POST['submit-button'])) {
            $receiver = Session::getUser();
            $sender = new User(null, $_POST['sender'], null);
            $group = new Group(null, $_POST['group'], null);
            
            $notification = NotificationsService::getByReceiverAndGroup($receiver,$group);

            if(isset($notification)) {
                $group = $notification->group;

                if ($_POST['submit-button'] == 'accept') {
                    MembersService::join($receiver,$group);
                    NotificationsService::delete($notification);
                } else if ($_POST['submit-button'] == 'decline') {
                    NotificationsService::delete($notification);
                }
            } 
        }
        header("Location: usersettings");
    }

    public function inviteToGroup()
    {
        if (isset($_POST['add-notification-button'])) {
            $user = AccountsService::exists($_POST['username']);

            if ($user === false) {
                // User does not exist
            } else if ($user->id !== Session::getUser()->id)
            {
                $group = new Group($_POST['group'], null, null);
                $notification = new Notification(null, $group, $user);
                if(!NotificationsService::exists($notification)){
                    NotificationsService::add($notification);
                }
            }
        }
        header("Location: usersettings");
    }
}