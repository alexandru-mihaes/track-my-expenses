<?php

class EditTransaction extends Controller {
    public function index(){
        $groups = GroupsService::getList(Session::getUser());
        $categories = CategoriesService::getList();
        $this->view('edit-transaction',[$groups,$categories]);
    }

    public function verifyId(){
        if (isset($_GET["id"])){
            $transaction = TransactionsService::getByID($_GET["id"]);
            if($transaction->user!=Session::getUser()->id) {
                return false;
            }
            return true;
        }
        else{
            return false;
        }
    }

    public function update() {
        if (isset($_POST['submit'])) {
            $transaction = new Transaction();
            $conn = Database::getConnection();

            $transaction->id=mysqli_real_escape_string($conn,$_GET["id"]);
            $transaction->title = mysqli_real_escape_string($conn,$_POST["title"]);
            $transaction->message = mysqli_real_escape_string($conn,$_POST["message"]);
            $transaction->amount = mysqli_real_escape_string($conn,$_POST["amount"]);
            $transaction->type = mysqli_real_escape_string($conn,$_POST["type"]);

            $transaction->user = Session::getUser()->id;
            $transaction->group = mysqli_real_escape_string($conn,$_POST["group"]);
            $transaction->category = mysqli_real_escape_string($conn,$_POST["category"]);

            $transaction->date = mysqli_real_escape_string($conn,$_POST["date"]);

            TransactionsService::edit($transaction);
        }
        header("Location: wallet");
    }

    public function remove() {
        if (isset($_POST['submit'])){
            TransactionsService::remove($_GET["id"]);
        }
        header("Location: wallet");
    }

    public function setDefaultDropdows($html,$transaction) {
        $dom = new DOMDocument();
        // Convert html in dom - nu afisam warninguri-le in legatura cu taguri neacceptata
        libxml_use_internal_errors(true);
        $dom->loadHTML($html);
        libxml_clear_errors();
        
        // Setam default pentru type
        $element = $dom->getElementById("T" . $transaction->type);
        if($element!=null){
            $element->setAttribute("selected","selected");
        }

        // Setam default pentru grup
        $element = $dom->getElementById("G" . $transaction->group);
        if($element!=null){
            $element->setAttribute("selected","selected");
        }

        // Setam default pentru categorie
        $element = $dom->getElementById("C" . $transaction->category);
        if($element!=null){
            $element->setAttribute("selected","selected");
        }

        return $dom;
    }
}