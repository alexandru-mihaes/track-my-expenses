<?php

class Database {
    static private $server = "localhost";
    static private $username = "root";
    static private $password = "";
    static private $dbName = "TME";

    static private $conn;

    static function getConnection() {
        if(isset(self::$conn)) {
            return self::$conn;
        } else {
            self::$conn = mysqli_connect(self::$server, self::$username, self::$password, self::$dbName);
            return self::$conn;
        }
    }
}