<?php

class Route {

    public static $validRoutes = array();

    public static function set($route, $function){
        self::$validRoutes[] = $route;
        
        $url = 'home';

        if(isset($_GET['url'])){
            $url=$_GET['url'];
        }

        if($url==$route){
            $GLOBALS['routeFound'] = true;
            $function->__invoke();
        }
    }
}