<?php

class Session {
    private static function resume() {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public static function signin($user) {
        Session::resume();

        $_SESSION["authentificated"] = true;
        $_SESSION["user"] = $user;
    }
    public static function signout() {
        Session::resume();

        $_SESSION["authentificated"] = false;
    }

    public static function getUser() {
        Session::resume();
        return $_SESSION["user"];
    }

    public static function isAuthentificated() {
        Session::resume();

        if (isset($_SESSION["authentificated"]) && $_SESSION["authentificated"] == "true") {
            $authentificated = true;
        } else {
            $authentificated = false;
        }

        return $authentificated;
    }
}