<?php

require_once '../../app/models/User.php';
require_once '../../app/models/Group.php';
require_once '../../app/models/Category.php';
require_once '../../app/models/Transaction.php';
require_once '../../app/models/Date.php';
require_once '../../app/core/Session.php';
require_once '../../app/core/Database.php';
require_once '../../app/services/TransactionsService.php';
require_once '../../app/services/GroupsService.php';
require_once '../../app/services/CategoriesService.php';
require_once '../../app/views/ComponentFactory.php';
require_once '../../app/services/AccountsService.php';

$user = Session::getUser();
$group = GroupsService::getByName($user,$_REQUEST['group']);
$category = CategoriesService::getByName($_REQUEST['category']);

$transactions = TransactionsService::getByFilters($user,$group,$category);
$html = ComponentFactory::getTransactionData($transactions);

echo $html;