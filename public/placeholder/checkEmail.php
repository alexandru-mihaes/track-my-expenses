<?php

require_once '../../app/core/Database.php';
require_once '../../app/services/AccountsService.php';

$email = $_REQUEST['email'];
$available = AccountsService::isEmailAvailable($email);

if($available) {
    echo 'true';
} else {
    echo 'false';
}