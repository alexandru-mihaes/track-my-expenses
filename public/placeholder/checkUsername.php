<?php

require_once '../../app/core/Database.php';
require_once '../../app/services/AccountsService.php';

$username = $_REQUEST['username'];
$available = AccountsService::isUserAvailable($username);

if($available) {
    echo 'true';
} else {
    echo 'false';
}