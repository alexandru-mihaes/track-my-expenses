var urlParams = new URLSearchParams(window.location.search);
var groupValue = urlParams.get("group") ? urlParams.get("group") : "Personal";
var categoryValue = urlParams.get("category") ? urlParams.get("category") : "all";

window.onload = function () {
    (document.getElementsByClassName("change-view"))[0].addEventListener("click", displayDate);

    var tempArray = window.location.href.split("?");
    window.history.replaceState('', '', tempArray[0]);

    getTransactionData();
    listenRadios("group");
    listenRadios("category");
};

function listenRadios(radioName) {
    var radios = document.getElementsByName([radioName]);
    for (var iterator = 0; iterator < radios.length; iterator++) {
        radios[iterator].onclick = function () {
            if(radioName === 'category')
                categoryValue = this.value;
            else groupValue = this.value;
            setNewParams();
        }
    }

}

function setNewParams() {
    window.history.replaceState('', '', updateURLParameter(window.location.href, 'group', groupValue));
    window.history.replaceState('', '', updateURLParameter(window.location.href, 'category', categoryValue));

    getTransactionData();
}
function getTransactionData() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("transaction-data").innerHTML = this.responseText;
        }
    };
    xmlhttp.open("GET", "placeholder/getTransactionData.php?group=" + groupValue + "&category=" + categoryValue, true);
    xmlhttp.send();
}

function updateURLParameter(url, param, paramVal) {
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (var i = 0; i < tempArray.length; i++) {
            if (tempArray[i].split('=')[0] != param) {
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }
    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

function displayDate() {
    alert("We should change the view...\n" +
        "Also, having a button is optional ;)");
}