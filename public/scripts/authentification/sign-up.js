function validateForm() {
    var message = document.getElementById("confirmMessage");
    
    var email = document.getElementById("email");
    checkEmail(email.value,false);
    if(message.className === "bad") return false;

    var username = document.getElementById("username");
    checkUsername(username.value,false);
    if(message.className === "bad") return false;

    matchPassword();
    if(message.className === "bad") return false;
    
    form = document.getElementById("sign-up-form");
    form.submit();
    return true;
}

function checkUsername(username,async = true) {
    var message = document.getElementById("confirmMessage");

    if (username.length == 0) {
        setBadMessage("Please type a username", message);
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText === "true") {
                    setGoodMessage(username + " is available", message);
                } else {
                    setBadMessage(username + " is taken", message);
                }
            }
        };
        xmlhttp.open("GET", "placeholder/checkUsername.php?username=" + username, async);
        xmlhttp.send();
    }
}

function checkEmail(email,async = true) {
    var message = document.getElementById("confirmMessage");

    if (email.length == 0) {
        setBadMessage("Please type an email", message);
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                if (this.responseText === "true") {
                    setGoodMessage("Email " + email + " is available", message);
                } else {
                    setBadMessage("Email " + email + " is used", message);
                }
            }
        };
        xmlhttp.open("GET", "placeholder/checkEmail.php?email=" + email, async);
        xmlhttp.send();
    }
    return false;
}

function matchPassword() {
    var message = document.getElementById("confirmMessage");
    var password = document.getElementById("password").value;
    var confirmPassword = document.getElementById("confirmPassword").value;

    if (password.length == 0) {
        setBadMessage("Please type a password",message);
    } else if(confirmPassword.length == 0) {
        setBadMessage("Please confirm your password",message);
    } else {
        if(password === confirmPassword) {
            setGoodMessage("Passwords match",message);
        } else {
            setBadMessage("Passwords do not match",message);
        }
    }
}

function setGoodMessage(content, container, replaceBadMessage = true) {
    if(replaceBadMessage === false && container.className === "bad")
        return;
    container.innerHTML = content;
    container.className = "good";
}

function setBadMessage(content, container) {
    container.innerHTML = content;
    container.className = "bad";
}
