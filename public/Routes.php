<?php

$GLOBALS['routeFound'] = false;

Route::set('home', function(){
    $controller = new Home();
    $controller->index();
});

Route::set('wallet', function(){
    $controller = new Wallet();
    $controller->index();
});

Route::set('signin', function(){
    $controller = new SignIn();
    $controller->index();
});

Route::set('signin-action', function(){
    $controller = new SignIn();
    $controller->signin();
});

Route::set('signup', function(){
    $controller = new SignUp();
    $controller->index();
});

Route::set('signup-action', function(){
    $controller = new SignUp();
    $controller->register();
});

Route::set('forgotpass', function(){
    $controller = new ForgotPass();
    $controller->index();
});

Route::set('addtransaction', function(){
    $controller = new AddTransaction();
    $controller->index();
});

Route::set('edittransaction', function(){
    $controller = new EditTransaction();
    if($controller->verifyId()){
        $controller->index();
    }
    else {
        header ("Location: wallet");
    }
});

Route::set('update-transaction', function(){
    $controller = new EditTransaction();
    $controller->update();
});

Route::set('signout', function(){
    $controller = new Signout();
    $controller->index();
});

Route::set('addtransaction-action', function(){
    $controller = new AddTransaction();
    $controller->submit();
});

Route::set('remove', function(){
    $controller = new EditTransaction();
    $controller->remove();
});

Route::set('usersettings', function(){
    $controller = new UserSettings();
    $controller->index();
});

/* Actions in user-settings */

Route::set('usersettings-addgroup', function(){
    $controller = new UserSettings();
    $controller->addGroup();
});

Route::set('usersettings-deletegroup', function(){
    $controller = new UserSettings();
    $controller->deleteGroup();
});

Route::set('usersettings-addcategory', function(){
    $controller = new UserSettings();
    $controller->addCategory();
});

Route::set('usersettings-deletecategory', function(){
    $controller = new UserSettings();
    $controller->deleteCategory();
});

Route::set('notification-action', function() {
    $controller = new UserSettings();
    $controller->answerNotification();
});

Route::set('invite-to-group', function() {
    $controller = new UserSettings();
    $controller->inviteToGroup();
});

if(!$GLOBALS['routeFound']){
    header("Location: http://localhost/track-my-expenses/public/home");
}